'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Cliente = use('App/Models/Cliente')
const Ponto = use('App/Models/Ponto')


/**
 * Resourceful controller for interacting with clientes
 */
class ClienteController{
  
  async index ({response, request}) {
    //obtem um objeto com os parametros da consulta
    const dados = request.get()
    const count = await Cliente.query().where(dados).whereNull('data_remocao').getCount()
    if(count > 0){
      const cliente = await Cliente.query().select('*').where(dados).whereNull('data_remocao').fetch()
      await Cliente.query().where(dados).update({ data_remocao :  null})
      return response.status(201).json({dados : cliente})
    }
    try{
      //ler apenas os cliente com a coluna data_remocao == null
      const clientes = await Cliente.query().select('*').whereNull('data_remocao').fetch()
      return response.status(200).json({dados : clientes})
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }

  /**
   * Create/save a new cliente.
   * POST clientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const dados = request.post()
    //Verifica se no banco tem algum cliente salvo com as informações e o data_remocao não nulo
    const count = await Cliente.query().where(dados).whereNotNull('data_remocao').getCount()
    if(count > 0){
      const cliente = await Cliente.query().select('*').where(dados).whereNotNull('data_remocao').fetch()
      await Cliente.query().where(dados).update({ data_remocao :  null})
      return response.status(201).json(cliente)
    }

    try{
      const cliente_aux = await Cliente.create(dados)
      return response.status(201).json(cliente_aux)
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }

  /**
   * Display a single cliente.
   * GET clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({params, response}) {
    try{
      const cliente = await Cliente.findOrFail(params.id)
      return response.status(200).json({nome: cliente.nome, tipo : cliente.tipo})
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }

  /**
   * Update cliente details.
   * PUT or PATCH clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response}) {
    try{
      const cliente = await Cliente.findOrFail(params.id)
      const dados = request.post()
      cliente.merge(dados)

      await cliente.save()

      return response.status(204).json(cliente)
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }

  /**
   * Delete a cliente with id.
   * DELETE clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async edit ({ params, response }) {
     try{
      const cliente = await Cliente.findOrFail(params.id)
      cliente.data_remocao = new Date()
      await cliente.save()
      await Ponto.query().where('cliente_id', cliente.id).update({data_remocao : cliente.data_remocao})
      return response.status(204).json({})
     }
     catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }
}

module.exports = ClienteController