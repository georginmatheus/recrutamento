'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const Contrato = use('App/Models/Contrato')
const Ponto = use('App/Models/Ponto')
const ContratoEvento = use('App/Models/ContratoEvento')

const Database = use('Database')
/**
 * Resourceful controller for interacting with contratoes
 */
class ContratoController {
  async index ({response, request}) {
    
    const dados = request.get()

    try{
        const pontos = await Database.select('t_contrato.id', 't_cliente.id as cliente_id', 't_cliente.nome as cliente_nome', 
                                            't_cliente.tipo as cliente_tipo', 't_endereco.id as endereco_id', 
                                            't_endereco.logradouro as endereco_logradouro', 't_endereco.bairro as endereco_bairro',
                                            't_endereco.numero as endereco_numero')
                                    .from('t_ponto').where(dados).whereNull('t_ponto.data_remocao')
                                    .innerJoin('t_endereco', 't_ponto.endereco_id', "t_endereco.id")
                                    .innerJoin('t_cliente', 't_ponto.cliente_id', "t_cliente.id")
                                    .innerJoin('t_contrato', 't_ponto.id', "t_contrato.ponto_id")

      return response.status(200).json({dados : pontos})
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }

  /**
   * Create/save a new contrato.
   * POST contratos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const dados = request.post()
    //Verifica se no banco tem algum contrato salvo com as informações e o data_remocao não nulo
    
    const ponto = await Ponto.find(dados.ponto_id)
    if(ponto.data_remocao != null){
      return response.status(400).json({error : "Ponto desativado"})
    }

    const count = await Contrato.query().where(dados).whereNotNull('data_remocao').getCount()
    if(count > 0){
      const contrato = await Contrato.query().select('*').where(dados).whereNotNull('data_remocao').fetch()
      await Contrato.query().where(dados).update({ data_remocao :  null})
      return response.status(201).json(contrato)
    }

    try{
      const contrato_aux = await Contrato.create({estado : 'Em vigor', ponto_id : dados.ponto_id})
      return response.status(201).json(contrato_aux)
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }

  /**
   * Display a single contrato.
   * GET contratos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({params, response}) {
    try{
        const pontos = await Database.select('t_ponto.id', 't_cliente.id as cliente_id', 't_cliente.nome as cliente_nome', 't_cliente.tipo as cliente_tipo',
                                            't_endereco.id as endereco_id', 't_endereco.logradouro as endereco_logradouro', 't_endereco.bairro as endereco_bairro',
                                            't_endereco.numero as endereco_numero')
                                    .from('t_ponto')
                                    .where('t_ponto.id', params.id).whereNull('t_ponto.data_remocao')
                                    .innerJoin('t_endereco', 't_ponto.endereco_id', "t_endereco.id", )
                                    .innerJoin('t_cliente', 't_ponto.cliente_id', "t_cliente.id", )
      return response.status(200).json({dados : pontos})
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }

  async update ({ params, request, response}) {
    try{
      const contrato = await Contrato.findOrFail(params.id)
      const dados = request.post()
      const estado_anterior = contrato.estado
      if(dados.estado == 'Desativado Temporario' && contrato.estado == 'Em vigor'){
        contrato.merge(dados)
      }
      else if(contrato.estado == 'Cancelado'){
        return response.status(400).json({})
      }
      if(contrato.estado == 'Desativado Temporario' && dados.estado != 'Desativado Temporario'){
        contrato.merge(dados)
      }
      await ContratoEvento.create({estado_anterior, estado_posterior : dados.estado, contrato_id: params.id})
      await contrato.save()

      return response.status(204).json(contrato)
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }

  /**
   * Delete a contrato with id.
   * DELETE contratos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async edit ({ params, response }) {
     try{
      const contrato = await Contrato.findOrFail(params.id)
      contrato.data_remocao = new Date()
      await contrato.save()
      return response.status(204).json({})
     }
     catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }
}

module.exports = ContratoController
