'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const ContratoEvento = use('App/Models/ContratoEvento')
/**
 * Resourceful controller for interacting with contratoeventos
 */
class ContratoEventoController {
  async index ({params, response}) {
    try{
      const eventos = await ContratoEvento.query().select('t_contrato_evento.id as id',
                                                         't_contrato_evento.data_criacao as data_evento',
                                                         't_contrato_evento.estado_anterior as estado_antigo',
                                                         't_contrato_evento.estado_posterior as estado_novo' )
                                                .where('t_contrato_evento.contrato_id', params.id)
                                                .fetch()

    
      return response.status(200).json({dados : eventos})
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }
}

module.exports = ContratoEventoController
