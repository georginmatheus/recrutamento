'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Endereco = use('App/Models/Endereco')
const Ponto = use('App/Models/Ponto')

/**
 * Resourceful controller for interacting with enderecos
 */
class EnderecoController {
  async index ({response, request}) {
    const dados = request.get()
    const count = await Endereco.query().where(dados).whereNull('data_remocao').getCount()

    if(count > 0){
      const endereco = await Endereco.query().select('*').where(dados).whereNull('data_remocao').fetch()
      return response.status(200).json({dados : endereco})
    }

    try{
      const enderecos = await Endereco.query().select('*').whereNull('data_remocao').fetch()
      return response.status(200).json({dados : enderecos})
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }

  /**
   * Create/save a new endereco.
   * POST enderecos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const dados = request.post()
    //Verifica se no banco tem algum endereco salvo com as informações e o data_remocao não nulo
    const count = await Endereco.query().where(dados).whereNotNull('data_remocao').getCount()
    if(count > 0){
      const endereco = await Endereco.query().select('*').where(dados).whereNotNull('data_remocao').fetch()
      await Endereco.query().where(dados).update({ data_remocao :  null})
      return response.status(201).json(endereco)
    }
    //verifica se o endereço já foi cadastrado
    const aux = await Endereco.query().where(dados).whereNull('data_remocao').getCount()
    if(aux > 0){
      return response.status(400).json({error : "Endereço já cadastrado"})
    }
    

    try{
      const endereco_aux = await Endereco.create(dados)
      return response.status(201).json(endereco_aux)
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }

  /**
   * Display a single endereco.
   * GET enderecos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({params, response}) {
    try{
      const endereco = await Endereco.findOrFail(params.id)
      return response.status(201).json({logradouro: endereco.logradouro, bairro : endereco.bairro, numero: endereco.numero})
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }

  /**
   * Update endereco details.
   * PUT or PATCH enderecos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response}) {
    const dados = request.get()
    const aux = await Endereco.query().where(dados).whereNull('data_remocao').getCount()
    if(aux > 0){
      return response.status(400).json({error : "Endereço já cadastrado"})
    }
    try{
      const endereco = await Endereco.findOrFail(params.id)
      const dados = request.post()
      endereco.merge(dados)

      await endereco.save()

      return response.status(204).json(endereco)
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }

  /**
   * Delete a endereco with id.
   * DELETE enderecos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async edit ({ params, response }) {
     try{
      const endereco = await Endereco.findOrFail(params.id)
      endereco.data_remocao = new Date()
      await endereco.save()
      await Ponto.query().where('cliente_id', endereco.id).update({data_remocao : endereco.data_remocao})
      return response.status(204).json({})
     }
     catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }
}

module.exports = EnderecoController
