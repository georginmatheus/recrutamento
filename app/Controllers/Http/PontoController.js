'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Ponto = use('App/Models/Ponto')
const Cliente = use('App/Models/Cliente')
const Endereco = use('App/Models/Endereco')
const Contrato = use('App/Models/Contrato')
const Database = use('Database')


/**
 * Resourceful controller for interacting with Clientes
 */
class PontoController {
  async index ({response, request}) {
    const dados = request.get()
    try{
      const pontos = await Database.select('t_ponto.id', 't_cliente.id as cliente_id', 't_cliente.nome as cliente_nome', 
                                            't_cliente.tipo as cliente_tipo', 't_endereco.id as endereco_id', 
                                            't_endereco.logradouro as endereco_logradouro', 't_endereco.bairro as endereco_bairro',
                                            't_endereco.numero as endereco_numero')
                                    .from('t_ponto').where(dados).whereNull('t_ponto.data_remocao')
                                    .innerJoin('t_endereco', 't_ponto.endereco_id', "t_endereco.id", )
                                    .innerJoin('t_cliente', 't_ponto.cliente_id', "t_cliente.id", )
      return response.status(200).json({dados : pontos})
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
    
  }

  /**
   * Create/save a new ponto.
   * POST pontos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const dados = request.post()

    const cliente = await Cliente.find(dados.cliente_id)
    const endereco = await Endereco.find(dados.endereco_id)

    //Cliente ou endereço desativados?
    if(cliente.data_remocao != null){
      return response.status(400).json({error : "Cliente desativado"})
    }
    else if(endereco.data_remocao != null){
      return response.status(400).json({error : "Endereco desativado"})
    }
    //Ponto existe e está desativado?
    const count = await Ponto.query().where(dados).whereNotNull('data_remocao').getCount()
    if(count > 0){
      const ponto = await Ponto.query().select('*').where(dados).whereNotNull('data_remocao').fetch()
      await Ponto.query().where(dados).update({ data_remocao :  null})
      return response.status(201).json(ponto)
    }

    //Ponto existe e está ativado?
    const aux = await Ponto.query().where(dados).getCount()
    if(aux > 0){
      return response.status(400).json({error : "Ponto já cadastrado"})
    }
    
    //criando um novo ponto
    try{
      const ponto_aux = await Ponto.create(dados)
      return response.status(201).json(ponto_aux)
    }
    catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }

  /**
   * Delete a ponto with id.
   * DELETE pontos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async edit ({ params, response }) {
    try{
      const ponto = await Ponto.findOrFail(params.id)
      ponto.data_remocao = new Date()
      await ponto.save()
      await Contrato.query().where('ponto_id', ponto.id).update({data_remocao : ponto.data_remocao})
      
      return response.status(204).json({})
     }
     catch(err){
      return response.status(400).json({error: `Erro: ${err.message}`})
    }
  }
}

module.exports = PontoController
