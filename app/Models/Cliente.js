'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const moment = require("moment");

class Cliente extends Model {

    static get dates(){
        return super.dates.concat(['data_criacao', 'data_atualizacao', 'data_remocao'])
    }
    /*
    static get dateFormat () {
        return 'YYYY-MM-DD HH:mm:ss'
    }*/

    static get table(){
        return 't_cliente'
    }

    static get connection () {
        return 'pg'
    }

    static get createdAtColumn () {
        return 'data_criacao'
    }

    static get updatedAtColumn (){
        return 'data_atualizacao'
    }

    static get deleteTimestamp () {
        return 'data_remocao'
      }

}

module.exports = Cliente
