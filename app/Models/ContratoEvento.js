'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ContratoEvento extends Model {
    
    static get dates(){
        return super.dates.concat(['data_criacao', 'data_atualizacao'])
    }

    /*
    static get dateFormat () {
        return 'YYYY-MM-DD HH:mm:ss'
    }*/

    static get table(){
        return 't_contrato_evento'
    }

    static get connection () {
        return 'pg'
    }

    static get createdAtColumn () {
        return 'data_criacao'
    }

    static get updatedAtColumn (){
        return 'data_atualizacao'
    }
}

module.exports = ContratoEvento
