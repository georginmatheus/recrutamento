'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClientesSchema extends Schema {
  up () {
      this.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
      .create('t_cliente', (table) => {
        table.uuid('id').unsigned().notNullable().primary().defaultTo(this.db.raw('uuid_generate_v4()'))
        table.string('nome', 128).notNullable().unique()
        table.enu('tipo', ['jurídico', 'fisico', 'especial'], {useNative: true, enumName: 'tipo_cliente'})
        table.timestamp('data_remocao', { useTz: true}).defaultTo(null)
        table.timestamp('data_atualizacao').notNullable().defaultTo(this.fn.now())
        table.timestamp('data_criacao').notNullable().defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('t_cliente')
  }
}

module.exports = ClientesSchema
