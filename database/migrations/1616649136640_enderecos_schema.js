'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EnderecosSchema extends Schema {
  up () {
    this.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
    .create('t_endereco', (table) => {
      table.uuid('id').unsigned().notNullable().primary().defaultTo(this.db.raw('uuid_generate_v4()'))
      table.string('logradouro', 128).notNullable()
      table.string('bairro', 255).notNullable()
      table.integer('numero').notNullable()
      table.timestamp('data_remocao', { useTz: true}).defaultTo(null)
      table.timestamp('data_atualizacao').notNullable().defaultTo(this.fn.now())
      table.timestamp('data_criacao').notNullable().defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('t_endereco')
  }
}

module.exports = EnderecosSchema
