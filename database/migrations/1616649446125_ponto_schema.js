'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PontoSchema extends Schema {
  up () {
    this.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
    .create('t_ponto', (table) => {
      table.uuid('id').unsigned().notNullable().primary().defaultTo(this.db.raw('uuid_generate_v4()'))
      table.uuid('cliente_id').notNullable().unsigned().references('id').inTable('t_cliente').onUpdate('CASCADE').onDelete('CASCADE')
      table.uuid('endereco_id').notNullable().unsigned().references('id').inTable('t_endereco').onUpdate('CASCADE').onDelete('CASCADE')
      table.timestamp('data_remocao', { useTz: true}).defaultTo(null)
      table.timestamp('data_atualizacao').notNullable().defaultTo(this.fn.now())
      table.timestamp('data_criacao').notNullable().defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('t_ponto')
  }
}

module.exports = PontoSchema
