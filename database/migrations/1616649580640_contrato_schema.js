'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoSchema extends Schema {
  up () {
    this.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
    .create('t_contrato', (table) => {
      table.uuid('id').unsigned().notNullable().primary().defaultTo(this.db.raw('uuid_generate_v4()'))
      table.uuid('ponto_id').notNullable().unsigned().references('id').inTable('t_ponto').onUpdate('CASCADE').onDelete('CASCADE')
      table.enu('estado', ['Em vigor', 'Desativado Temporario', 'Cancelado'], {useNative: true, enumName: 'contrato_estado'}).notNullable()
      table.timestamp('data_remocao', { useTz: true}).defaultTo(null)
      table.timestamp('data_atualizacao').notNullable().defaultTo(this.fn.now())
      table.timestamp('data_criacao').notNullable().defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('t_contrato')
  }
}

module.exports = ContratoSchema
