'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoEventoSchema extends Schema {
  up () {
    this.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"')
    .create('t_contrato_evento', (table) => {
      table.uuid('id').unsigned().notNullable().primary().defaultTo(this.db.raw('uuid_generate_v4()'))
      table.uuid('contrato_id').notNullable().unsigned().references('id').inTable('t_contrato').onUpdate('CASCADE').onDelete('CASCADE')
      table.enu('estado_anterior', ['Em vigor', 'Desativado Temporario', 'Cancelado'], {useNative: true, existingType:true, enumName: 'contrato_estado'}).notNullable()
      table.enu('estado_posterior', ['Em vigor', 'Desativado Temporario', 'Cancelado'], {useNative: true,  existingType:true, enumName: 'contrato_estado'}).notNullable()
      table.timestamp('data_atualizacao').notNullable().defaultTo(this.fn.now())
      table.timestamp('data_criacao').notNullable().defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('t_contrato_evento')
  }
}

module.exports = ContratoEventoSchema
