'use strict'

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('/clientes', 'ClienteController.store')
Route.get('/clientes/(nome, tipo)?', 'ClienteController.index')
Route.put('/cliente/:id', 'ClienteController.update')
Route.get('/cliente/:id', 'ClienteController.show')
Route.delete('/cliente/:id', 'ClienteController.edit')

Route.post('/enderecos', 'EnderecoController.store')
Route.get('/enderecos/(logradouro, bairro, numero)?', 'EnderecoController.index')
Route.put('/endereco/:id', 'EnderecoController.update')
Route.get('/endereco/:id', 'EnderecoController.show')
Route.delete('/endereco/:id', 'EnderecoController.edit')

Route.post('/pontos', 'PontoController.store')
Route.get('/pontos/(cliente_id, endereco_id)?', 'PontoController.index')
Route.delete('/ponto/:id', 'PontoController.edit')

Route.post('/contratos', 'ContratoController.store')
Route.get('/contratos', 'ContratoController.index')
Route.put('/contrato/:id', 'ContratoController.update')
Route.get('/contrato/:id', 'ContratoController.show')
Route.delete('/contrato/:id', 'ContratorecoeController.edit')

Route.get('/contrato/:id/historico', 'ContratoEventoController.index')

/*
*/